import { composeWithTracker } from 'react-komposer';
import ScrimList from '../components/scrim/scrim_list.js';

function composer(props, onData) {
    const handle = Meteor.subscribe('scrims');
    if (handle.ready()) {
        const posts = Scrims.find({}, { sort: { date: 1 } }).fetch();
        onData(null, { posts });
    }
}

export default composeWithTracker(composer)(ScrimList);
