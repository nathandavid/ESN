Template.UserProfile.events({
	'click #like': function(e, tmpl) {
		alert('Youre a fan!');
	},

	'click #edit': function(e, tmpl) {
		FlowRouter.go('/user/' + FlowRouter.getParam('id') + '/edit');
		console.log('hi');
	},

	'click #commend': function(e, tmpl) {
		alert('Commended!');
	},

	'click #report': function(e, tmpl) {
		alert('Reported!');
	}
});

Template.UserProfile.helpers({
	user: function(){
		var userId = FlowRouter.getParam('id');
	    var user = Meteor.users.findOne({_id: userId}) || {};
	    return user;
	}
});

Template.UserProfile.onCreated(function() {
	var self = this;
	self.autorun(function() {
		var userId = FlowRouter.getParam('id');
    	self.subscribe('userProfiles', userId);
	});
});