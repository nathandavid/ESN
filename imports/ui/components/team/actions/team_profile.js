Template.TeamProfile.events({
	'click .leave': function(e) {
		e.preventDefault();

		var options = { 'profile.team': '', 'profile.teamSlug': '' };
		Meteor.call('leaveTeam', this._id, Meteor.user().username);
		Meteor.call('updateUser', Meteor.user()._id, options);
		
		$('#leaveTeamModal').modal('hide');
	},

	'click .delete': function(e) {
		e.preventDefault();

		var options = { 'profile.team': '', 'profile.teamSlug': '' };
		Meteor.call('removeTeam', this._id);
		Meteor.call('updateUser', Meteor.user()._id, options);

		$('#deleteTeamModal').modal('hide');
		FlowRouter.go('/');
	}
});

Template.TeamProfile.helpers({
	team: function() {
		return Teams.findOne({ url: FlowRouter.getParam("id") });
	},

	isOwner: function(){
		if (Meteor.user()) {
			return Meteor.user()._id === this.owner;
		}
	},

	// isMember: function(){
	// 	if (Meteor.user()) {
	// 		var members = this.members;

	// 		for (var i in members) {
	// 			if (members[i] === Meteor.user().username) {
	// 				return true;
	// 			}
	// 		}
	// 	}
	// }
});

Template.TeamProfile.onCreated(function() {
	var self = this;
	self.autorun(function() {
		var teamId = FlowRouter.getParam('id');
    	self.subscribe('teamProfiles', teamId);
	});
});