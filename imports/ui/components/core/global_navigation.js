import React from 'react';

class GlobalNavigation extends React.Component {
	render() {
		return (
			<div className="GlobalNav">
				<div className="GlobalNav-mast">
					ESN
				</div>

				<div className="GlobalNav-menu">
					<ul>
						<li><a href="/">home</a></li>
						<li><a href="/blog">blog</a></li>
						<li><a href="/about">about</a></li>
					</ul>
					games
					<ul>
						<li><a href="/">dota 2</a></li>
					<li><a href="/blog">CS:GO</a></li>
					</ul>
				</div>
			</div>
		);
	}
}

export default GlobalNavigation;
