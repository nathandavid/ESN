import React from 'react';

const FormError = ({ error }) => (
	<div className="FormError">
		<p>{error}</p>
	</div>
);

FormError.propTypes = {
	error: React.PropTypes.string.isRequired
};

export default FormError;
