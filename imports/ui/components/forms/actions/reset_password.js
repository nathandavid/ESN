Template.ResetPassword.events({
	'submit form': function(e, tmpl) {
		e.preventDefault();

		var token = FlowRouter.getParam('id');
		var password = tmpl.find('#password').value;
		var confirm = tmpl.find('#confirm').value;

		if (password === confirm) {
			Accounts.resetPassword(token, password, function(err){
				if (err) {
					Session.set('formError', err.reason);
				} else {
					//sAlert goes here
					FlowRouter.go('/signin');
					Session.set('formError', 'Password has been changed.');
				}
			});
		} else {
			Session.set('formError', 'Password mismatch.');
		}
	}
});