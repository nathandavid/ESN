import React from 'react';
import FormError from '../core/form_error';

const SignUp = () => (
	<div className="SignUp">
		<form className="AccountForms">
			<h2>Create your account</h2>
			<div className="AccountForms-group">
				<label htmlFor="email">Email*</label>
				<input type="email" id="email" required />
			</div>

			<div className="AccountForms-group AccountForms-halfInput">
				<label htmlFor="fname">First Name*</label>
				<input type="text" id="fname" required />
			</div>

			<div className="AccountForms-group AccountForms-halfInput">
				<label htmlFor="lname">Last Name*</label>
				<input type="text" id="lname" required />
			</div>

			<div className="AccountForms-group">
				<label htmlFor="handle">Handle*</label>
				<input type="text" id="handle" placeholder="In-game name" required />
			</div>

			<div className="AccountForms-group AccountForms-halfInput">
				<label htmlFor="password">Password*</label>
				<input type="password" id="password" required />
			</div>

			<div className="AccountForms-group AccountForms-halfInput">
				<label htmlFor="confirm">Confirm Password*</label>
				<input type="password" id="confirm" required />
			</div>

			<div className="AccountForms-group">
				<label htmlFor="steam">Steam Profile ID <span>(dota2, csgo)</span></label>
				<input type="text" id="steam" placeholder="http://steamcommunity.com/id/?" />
			</div>

			<FormError />

			<button type="submit" className="AccountForms--button">Create Account</button>
		</form>
	</div>
);

export default SignUp;
