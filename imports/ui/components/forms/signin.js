import React from 'react';
import FormError from '../core/form_error';

const SignIn = () => (
	<div className="SignIn">
		<form className="AccountForms">
			<h2>Sign in</h2>
			<div className="AccountForms-group">
				<label htmlFor="email">Email</label>
				<input type="email" id="email" required />
			</div>

			<div className="AccountForms-group">
				<label htmlFor="password">Password</label>
				<input type="password" id="password" required />
			</div>

			<FormError />

			<button type="submit" className="AccountForms--button">Sign in</button>
			<a href="/requestpassword" className="AccountsForms--forgotPass">Forgot password?</a>
		</form>
	</div>
);

export default SignIn;
