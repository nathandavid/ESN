import React from 'react';
import FormError from '../core/form_error';

const RequestPassword = () => (
    <div className="reset-password">
		<form className="account-forms">
			<h2>Reset Password</h2>
			<p>Create a new password.</p>

			<div className="form-group">
			<label htmlFor="password">New Password</label>
				<input type="password" id="password" required />
			</div>

			<div className="form-group">
			<label htmlFor="confirm">Confirm*</label>
				<input type="password" id="confirm" required />
			</div>

			<FormError />

			<button type="submit" className="button">Reset Password</button>
		</form>
	</div>
);

export default RequestPassword;
