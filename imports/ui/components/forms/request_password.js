import React from 'react';
import FormError from '../core/form_error';

const RequestPassword = () => (
    <div className="request-password">
        <form className="account-forms">
            <h2>Request Password Reset</h2>
            <p>After the request is sent, you will recieve an email with a link to reset your password.</p>

            <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="email" id="email" required />
            </div>

            <FormError />

            <button type="submit" className="button">Send Request</button>
        </form>
    </div>
);

export default RequestPassword;
