import React from 'react';
import ScrimItem from './scrim_item';

const ScrimList = () => (
    <div>
        This is the scrim list
        <ul>
            <li>
                {this.props.scrims.map((scrim) => {
                    return <ScrimItem meta={scrim} key={scrim._id} />;
                })}
            </li>
        </ul>
    </div>
);

export default ScrimList;
