// Later, store these session variabes in the users collection
Session.setDefault('dotaRegion', []);
Session.setDefault('dotaSkill', []);

Template.DotaScrimFilter.events({
	'click .region input[type=checkbox]': function(e, tmpl) {
		var region = tmpl.$('.region input:checked').map(function() {
			return $(this).val();
		});
		
		var regionArray = $.makeArray(region);
		Session.set('dotaRegion', regionArray);
		returnFilterQuery();
	},

	'click .skill input[type=checkbox]': function(e, tmpl) {
		var skill = tmpl.$('.skill input:checked').map(function() {
			return $(this).val();
		});
		
		var skillArray = $.makeArray(skill);
		Session.set('dotaSkill', skillArray);
		returnFilterQuery();
	}
});

function returnFilterQuery(){
	//check all fields and create a query object
	var query = {};

	var modifyQueryIfArray = function(key, sessionKey) {
	    var value = Session.get(sessionKey);
	    if (!_.isEmpty(value))
	     	query[key] = {$in: value};
  	};

	modifyQueryIfArray('region', 'dotaRegion');
	modifyQueryIfArray('skill', 'dotaSkill');

	console.log(query);
	return Session.set('dotaScrimFilter', query);
};