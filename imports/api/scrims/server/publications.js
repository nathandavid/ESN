Meteor.publish('scrims.dota', function() {
    const selector = { game: 'dota', removed: false };
    const options = {
		fields: {_id: 1, title: 1}
    };

    return Scrims.find(selector, options);
});

Meteor.publish('scrims.csgo', function() {
    const selector = { game: 'csgo', removed: false };
    const options = {
		fields: {_id: 1, title: 1}
    };

    return Scrims.find(selector, options);
});
