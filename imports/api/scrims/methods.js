Meteor.methods({
    'scrims.create'(_id, title, content) {
		check(_id, String);
		check(title, String);
		check(content, String);

		// Demo the latency compensations (Delete this in production)
		Meteor._sleepForMs(500);

		// XXX: Do some user authorization
		const createdAt = new Date();
		const post = { _id, title, content, createdAt };
		Scrims.insert(post);
    }
});
