Meteor.methods({
	resetPasswordEmail(id) {
		Accounts.sendResetPasswordEmail(id);
	}
});
