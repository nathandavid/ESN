import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Teams from '/teams/teams.js';

// We are using a composite publish because we want to search both the
// Places and Items collections simultaneously

// We are going to have to use ElasticSearch, as our search results need to be
// ranked and filtered.
export default function () {
	Meteor.publishComposite('search.public', function (query) {
		check(query, Match.OneOf(String, null, undefined));
		const result = new RegExp(query, 'i');

		return {
			find() {
				return Teams.find({
					name: result
				}, {
					fields: {
						checkins: 0,
						menu: 0
					}
				});
			},
			children: [
				{
					find() {
						return Meteor.users.find({
							name: result
						}, {
							fields: {
								comments: 0,
								photos: 0
							}
						});
					}
				}
			]
		};
	});
}
