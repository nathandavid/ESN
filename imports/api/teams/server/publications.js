Meteor.publish('teams.profiles', function(url) {
	new SimpleSchema({
    	url: {type: String}
	}).validate({ url });

    const selector = { url: url };
    const options = {};

    return Teams.find(selector, options);
});
