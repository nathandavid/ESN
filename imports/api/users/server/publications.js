Meteor.publish('users.data', function(email) {
	new SimpleSchema({
		email: { type: String }
	}).validate({ email });

    const selector = {
		emails: {
			$elemMatch: {
				'address': email
			}
		}
    };
    const options = {
		fields: { services: 0 }
    };

    return Meteor.users.find(selector, options);
});

Meteor.publish('users.profile', function(userId) {
	new SimpleSchema({
		userId: { type: String }
	}).validate({ userId });

    const selector = { _id: userId };
    const options = {
		fields: { services: 0 },
    };

    return Meteor.users.find(selector, options);
});
