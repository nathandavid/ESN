import { Accounts } from 'meteor/accounts-base';

Accounts.onLoginFailure(() => {
	console.log('Log in failed.');
});

Accounts.onCreateUser((options, user) => {
	// Generate a new user ID
	const newUser = user;
	newUser._id = Random.id();

	return newUser;
});
