Accounts.onLoginFailure(() => {
	Session.set('formError', 'Log in failed.');
});

Accounts.onResetPasswordLink((token) => {
	console.log('reset password link clicked.');
	console.log(token);
	FlowRouter.go(`/resetpassword/${token}`);
});
