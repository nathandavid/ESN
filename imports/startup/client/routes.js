import React from 'react';
import { mount } from 'react-mounter';
import MainLayout from '../../ui/components/core/main_layout.js';
import HomeView from '../../ui/views/home.js';
import SignInView from '../../ui/views/signin.js';
import SignUpView from '../../ui/views/signup.js';
import RequestPassword from '../../ui/views/request_pass.js';
import ResetPassword from '../../ui/views/reset_pass.js';
import TeamCreate from '../../ui/views/team_create.js';
import UserProfileView from '../../ui/views/user_profile.js';
import UserNotFound from '../../ui/views/user_not_found.js';
import TeamProfileView from '../../ui/views/team_profile.js';
import TeamNotFound from '../../ui/views/team_not_found.js';
import ScrimDota from '../../ui/views/scrim_dota.js';
import ScrimCSGO from '../../ui/views/scrim_csgo.js';

FlowRouter.route('/', {
    name: 'HomeView',
    action() {
        mount(MainLayout, {
            content: <HomeView />
        });
    }
});

FlowRouter.route('/signin', {
    name: 'SignInView',
    action() {
        mount(MainLayout, {
            content: <SignInView />
        });
    }
});

FlowRouter.route('/signup', {
    name: 'SignUpView',
    action() {
        mount(MainLayout, {
            content: <SignUpView />
        });
    }
});

FlowRouter.route('/requestpassword', {
    name: 'RequestPassword',
    action() {
        mount(MainLayout, {
            content: <RequestPassword />
        });
    }
});

FlowRouter.route('/resetpassword', {
    name: 'ResetPassword',
    action() {
        mount(MainLayout, {
            content: <ResetPassword />
        });
    }
});

FlowRouter.route('/newteam', {
    name: 'CreateTeam',
    action() {
        if (!Meteor.user()) {
            mount(MainLayout, {
                content: <SignInView />
            });
            Session.set('formError', 'You must be logged in to create a team.');
        } else {
            if (Meteor.user().profile.team.name) {
                FlowRouter.go(`/team/${Meteor.user().profile.team.url}`);
            } else {
                mount(MainLayout, {
                    content: <TeamCreate />
                });
            }
        }
    }
});

FlowRouter.route('/user/:id', {
    name: 'UserProfileView',
    action(params) {
        // Does this place exist?
        const user = Meteor.users.findOne(params.id);

        if (user) {
            mount(MainLayout, {
                content: <UserProfileView />
            });
        } else {
            mount(MainLayout, {
                content: <UserNotFound />
            });
        }
    }
});

FlowRouter.route('/team/:id', {
    name: 'TeamProfileView',
    action(params) {
        // Does this place exist?
        const team = Teams.findOne(params.id);

        if (team) {
            mount(MainLayout, {
                content: <TeamProfileView />
            });
        } else {
            mount(MainLayout, {
                content: <TeamNotFound />
            });
        }
    }
});

FlowRouter.route('/dota/scrim', {
    name: 'ScrimDota',
    action() {
        mount(MainLayout, {
            content: <ScrimDota />
        });
    }
});

FlowRouter.route('/csgo/scrim', {
    name: 'ScrimCSGO',
    action() {
        mount(MainLayout, {
            content: <ScrimCSGO />
        });
    }
});
